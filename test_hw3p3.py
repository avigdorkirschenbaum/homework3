# Sample solution
# Author: Michael Grossberg

import os
import re
import shutil
try:
    import unittest2 as unittest
except ImportError:
    import unittest
    

from subprocess import Popen, PIPE
from bs4 import BeautifulSoup 
import bs4

paragraph_text1 = """
April is the cruellest month, breeding
Lilacs out of the dead land, mixing
Memory and desire, stirring 
Dull roots with spring rain ...
"""

paragraph_text2 = """
The Chair she sat in, like a burnished throne,
Glowed on the marble, where the glass
Held up by standards wrought with fruited vines
From which a golden Cupidon peeped out
(Another hid his eyes behind his wing)
Doubled the flames of sevenbranched candelabra ...
"""

def stripper(text):
    """ Sripper takes a string as input. It 
    removes (1) all manner of whitespace 
    (2) commas (3) periods (4) different kinds
    of br tags and (5) p tags. This is useful
    for checking if the text content of 
    a p or h1-6 matches another while ignoring
    whitespace. 
    
    Should be redone at somepoint with regexp
    to be robust to style attributes and remove
    more internal tags.
    """
    return text.lower().replace('\n',''
                      ).replace('\f',''
                      ).replace('\v',''                      
                      ).replace('\t',''
                      ).replace('\r',''
                      ).replace(' ',''
                      ).replace('</br>',''
                      ).replace('<br/>',''
                      ).replace('<br>',''
                      ).replace(',',''
                      ).replace('.',''
                      ).replace('<p>',''
                      ).replace('</p>',''
                      )

 
class TestProblem2(unittest.TestCase):
        @classmethod
        def setUp(cls):
            """ Function that loads up the html file
            for testing. The file is processed
            using BeautifulSoup and made available
            to the testing through the soup variable.
            """
            cls.html = None
            with open("index.html") as fid:
                cls.html = fid.read()
                cls.soup = BeautifulSoup(cls.html)
            if not cls.html:
                raise Exception('File not read')
                
            
        @classmethod
        def tearDown(cls):
            """ Function for cleaning up after tests:
            You could delete index.html but
            not neccessary
            """
            pass

        # Fill in the tests below. For the ones you are checking
        # for specific content, you should probably "lower"-case
        # your string and the target string so case won't matter,
        # and use the stripper function to take out un-nessary tags

        def test_open_close_html(self):
            """ This test must check that there is exaclty one
            open and closing html tag
            """
            pass
        
        def test_head_in_html(self):
            """ This test must check that there is a single head
            tag and that it is inside the html open/close tags.
            """
            pass
            
        def test_body_in_html(self):
            """ This test must check that there is a single body
            tag and that it is inside the html open/close tags.
            """ 
            pass

        def test_meta(self):
            """ This test must check that there is meta tag
            with attribute 'charset' and value 'utf-8'
            """                        
            pass
                        
        def test_title(self):
            """ This checks that the html title is 
            'Text Example'.
            """
            pass

        def test_for_correct_doctype(self):
            """ This checks that the doctype is
            present and that its html
            """
            pass

            

        def test_h1_good_contents(self):
            """ This checks that there is a single 
            h1 tag present and that its contents is
            'Text Example' (no quotation marks).
            """
            pass

        def test_h2_good_contents(self):
            """ This checks that there is only one h2
            and that it has 'T. S. Eliot: THE WASTE LAND'
            in it. 
            """
            pass

        def test_h3_good_contents(self):
            """ This checks that the first of two h3
            contains 'I. THE BURIAL OF THE DEAD' and the
            second h3 has 'II. A GAME OF CHESS'
            in it.
            """
            pass
            
            
        def test_p1_good_contents(self):
            """ This compares the first paragraph to
            the string paragraph_text1 after striping and
            lowercasing
            """
            pass


        def test_p2_good_contents(self):
            """ This compares the first paragraph to
            the string paragraph_text2 after striping and
            lowercasing
            """            
            pass
        

        def test_breaks(self):
            """ For this one you are checking that each
            of the lines in the verse that is suppose
            to break has a break tag. You probably
            should list the words you expect need to 
            end with a break tag and use regular
            expressions to allow for whitespace
            between the break tag and the last word.
            Rememver the last sentence will break because
            of the <p> so it doesn't need a break.
            """
            pass
            

             